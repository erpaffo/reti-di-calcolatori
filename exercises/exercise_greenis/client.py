import redis
import time

r = redis.Redis(host="127.0.0.1", port=7379)

print("r.get(forever): ", r.get("forever"))

print('r.set("forever", "value"): ', r.set("forever", "value"))

print('r.set("key", "value", ex=10): ', r.set("key", "value", ex=10))

for i in range(12):
    print('r.get("key"): ', r.get("key"), "i = ", i)
    time.sleep(1)

print('r.get("non-existing-key"): ', r.get("non-existing-key"))
