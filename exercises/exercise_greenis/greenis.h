#ifndef GREENIS_H
#define GREENIS_H

#include <time.h> 

// Define the size of the hash table used to store keys and values.
#define TABLE_SIZE 128
#define BUFFER_SIZE 4096 // Buffer size for communication
#define PORT 7379 // Defines the default TCP port number for the server to listen on.

// Structure for hash table entries. Each entry represents a key-value pair.
typedef struct Entry {
    char* key;       // Key of the key-value pair
    char* value;     // Value associated with the key
    time_t expires;  // Expiration timestamp for the key (0 if it does not expire)
    struct Entry* next; // Pointer to the next entry in case of collision
} Entry;

/**
 * Computes the hash of a key.
 * 
 * @param key The key to hash.
 * @return The computed hash index.
 */
unsigned int hash(char* key);

/**
 * Inserts or updates a key with a value and an optional expiration time.
 * 
 * @param key The key to insert or update.
 * @param value The value associated with the key.
 * @param expire_seconds The number of seconds after which the key should expire. If 0, the key never expires.
 */
void insert(char* key, char* value, int expire_seconds);

/**
 * Retrieves the value associated with a key.
 * 
 * @param key The key whose value to retrieve.
 * @return The value associated with the key, or NULL if the key does not exist or has expired.
 */
char* get(char* key);

/**
 * Processes a command received from a client.
 * 
 * @param clientSock The client socket from which to receive the command.
 * @param command The command to process.
 */
void processRESPCommand(int clientSock, char* command);

/**
 * Handles a connection with a client, processing received commands.
 * 
 * @param clientSock The client socket to handle.
 */
void processClientConnection(int clientSock);

#endif // GREENIS_H
