#include "greenis.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

// Initialize the hash table to NULL for each entry.
Entry* table[TABLE_SIZE] = {NULL};

unsigned int hash(char* key) {
    unsigned long int value = 0;
    while (*key != '\0') {
        value = value * 37 + *key++;
    }
    return value % TABLE_SIZE;
}

void insert(char* key, char* value, int expire_seconds) {
    // Inserts a new key-value pair into the hash table or updates an existing one
    // Expire_seconds is used to set the expiration time (in client.py: ex=10 is expire_seconds)
    unsigned int slot = hash(key);
    Entry* entry = table[slot];
    time_t now = time(NULL);
    time_t expiration = expire_seconds > 0 ? now + expire_seconds : 0;

    while (entry != NULL) {
        if (strcmp(entry->key, key) == 0) {
            free(entry->value);
            entry->value = strdup(value);
            entry->expires = expiration;
            return;
        }
        entry = entry->next;
    }

    Entry* newEntry = (Entry*)malloc(sizeof(Entry));
    newEntry->key = strdup(key);
    newEntry->value = strdup(value);
    newEntry->expires = expiration;
    newEntry->next = table[slot];
    table[slot] = newEntry;
}

char* get(char* key) {
    // Retrieves the value for a key from the hash table if it has not expired
    unsigned int slot = hash(key);
    Entry* entry = table[slot];
    time_t now = time(NULL);

    while (entry != NULL) {
        if (strcmp(entry->key, key) == 0) {
            if (entry->expires == 0 || entry->expires > now) {
                return entry->value;
            } else {
                return NULL;
            }
        }
        entry = entry->next;
    }
    return NULL;
}

void processRESPCommand(int clientSock, char* command) {
    // Parses and processes a command from the client, handling SET and GET commands
    char *token, *cmd, *key, *value = NULL;
    int numArgs, expireSeconds = 0;
    token = strtok(command, "\r\n");
    sscanf(token, "*%d", &numArgs);
    for (int i = 0; i < numArgs; ++i) {
        token = strtok(NULL, "\r\n");
        if (i == 0) {
            cmd = strtok(NULL, "\r\n");
        } else if (i == 1) {
            key = strtok(NULL, "\r\n");
        } else if (i == 2 && strcmp(cmd, "SET") == 0) {
            value = strtok(NULL, "\r\n");
        } else if (i == 3 && strcmp(cmd, "SET") == 0 && numArgs > 4) {
            // Handles the expiration time for SET
            token = strtok(NULL, "\r\n");
            if (strcmp(token, "EX") == 0) {
                strtok(NULL, "\r\n");
                token = strtok(NULL, "\r\n");
                expireSeconds = atoi(token);
            }
            
        }
    }

    if (strcmp(cmd, "SET") == 0 && key && value) {
        insert(key, value, expireSeconds);
        send(clientSock, "+OK\r\n", 5, 0);
    } else if (strcmp(cmd, "GET") == 0 && key) {
        char* retrievedValue = get(key);
        if (retrievedValue) {
            char response[1024];
            int len = snprintf(response, sizeof(response), "$%ld\r\n%s\r\n", strlen(retrievedValue), retrievedValue);
            send(clientSock, response, len, 0);
        } else {
            send(clientSock, "$-1\r\n", 5, 0);
        }
    } else {
        send(clientSock, "-ERR Unsupported Command\r\n", 26, 0);
    }
}

void processClientConnection(int clientSock) {
    // Handles incoming commands from a client until the connection is closed
    char buffer[BUFFER_SIZE];
    while (1) {
        memset(buffer, 0, BUFFER_SIZE);
        ssize_t bytesReceived = recv(clientSock, buffer, BUFFER_SIZE - 1, 0);
        if (bytesReceived <= 0) break;

        buffer[bytesReceived] = '\0';
        processRESPCommand(clientSock, buffer);
    }
    close(clientSock);
}

int main() {
    int server_fd, client_fd;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    bind(server_fd, (struct sockaddr *)&address, sizeof(address));
    listen(server_fd, 3);
    printf("Greenis server listening on port %d\n", PORT);

    while ((client_fd = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))) {
        if (!fork()) {
            close(server_fd);
            processClientConnection(client_fd);
            exit(0);
        }
        close(client_fd);
    }

    return 0;
}
