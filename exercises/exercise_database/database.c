#include "database.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// Definition of value type for pseudo-polymorphism
typedef enum {
    INT,
    STRING
} ValueType;

// Unified IndexNode structure to support both int and string
typedef struct IndexNode {
    ValueType type;
    union {
        int intValue;
        char *stringValue;
    } value;
    struct IndexNode *left, *right;
    Persona *persona;
} IndexNode;

// Utility function to create a new index node for strings
IndexNode* createIndexNodeString(char *value, Persona *persona) {
    IndexNode *node = (IndexNode *)malloc(sizeof(IndexNode));
    node->type = STRING;
    node->value.stringValue = strdup(value); // Duplica la stringa per l'allocazione dinamica
    node->left = node->right = NULL;
    node->persona = persona;
    return node;
}

// Utility function to create a new index node for integers
IndexNode* createIndexNodeInt(int value, Persona *persona) {
    IndexNode *node = (IndexNode *)malloc(sizeof(IndexNode));
    node->type = INT;
    node->value.intValue = value;
    node->left = node->right = NULL;
    node->persona = persona;
    return node;
}

// Recursive function to insert into a binary search tree for strings
void insertIntoIndexString(IndexNode **root, char *value, Persona *persona) {
    if (*root == NULL) {
        *root = createIndexNodeString(value, persona);
    } else if (strcmp(value, (*root)->value.stringValue) < 0) {
        insertIntoIndexString(&(*root)->left, value, persona);
    } else {
        insertIntoIndexString(&(*root)->right, value, persona);
    }
}

// Recursive function to insert into a binary search tree for integers
void insertIntoIndexInt(IndexNode **root, int value, Persona *persona) {
    if (*root == NULL) {
        *root = createIndexNodeInt(value, persona);
    } else if (value < (*root)->value.intValue) {
        insertIntoIndexInt(&(*root)->left, value, persona);
    } else {
        insertIntoIndexInt(&(*root)->right, value, persona);
    }
}

// Inserts a person into the database
void insert(Database *database, Persona *person) {
    Persona *newPerson = (Persona *)malloc(sizeof(Persona));
    memcpy(newPerson, person, sizeof(Persona));

    insertIntoIndexString((IndexNode**)&database->name, newPerson->name, newPerson);
    insertIntoIndexString((IndexNode**)&database->surname, newPerson->surname, newPerson);
    insertIntoIndexString((IndexNode**)&database->address, newPerson->address, newPerson);
    insertIntoIndexInt((IndexNode**)&database->age, newPerson->age, newPerson);
}

// Recursive search function in a tree of strings
Persona* findInIndexString(IndexNode* node, char* value) {
    if (node == NULL) return NULL;
    int cmp = strcmp(value, node->value.stringValue);
    if (cmp == 0) return node->persona; // If found, return the person
    else if (cmp < 0) return findInIndexString(node->left, value); // Search left subtree
    else return findInIndexString(node->right, value); // Search right subtree
}

// Recursive search function in a tree of integers
Persona* findInIndexInt(IndexNode* node, int value) {
    if (node == NULL) return NULL;
    if (value == node->value.intValue) return node->persona; // If found, return the person
    else if (value < node->value.intValue) return findInIndexInt(node->left, value); // Search left subtree
    else return findInIndexInt(node->right, value); // Search right subtree
}

// Finds a person by name
Persona* findByName(Database *database, char *name) {
    return findInIndexString((IndexNode*)database->name, name);
}

// Finds a person by surname
Persona* findBySurname(Database *database, char *surname) {
    return findInIndexString((IndexNode*)database->surname, surname);
}

// Finds a person by address
Persona* findByAddress(Database *database, char *address) {
    return findInIndexString((IndexNode*)database->address, address);
}

// Finds a person by age
Persona* findByAge(Database *database, int age) {
    return findInIndexInt((IndexNode*)database->age, age);
}

// Function to free memory of a tree of string nodes
void freeIndexNodeString(IndexNode* node) {
    if (node != NULL) {
        freeIndexNodeString(node->left); // Recursively free left subtree
        freeIndexNodeString(node->right); // Recursively free right subtree
        free(node->value.stringValue); // Free the string value
        free(node); // Free the node itself
    }
}

// Function to free memory of a tree of integer nodes
void freeIndexNodeInt(IndexNode* node) {
    if (node != NULL) {
        freeIndexNodeInt(node->left); // Recursively free left subtree
        freeIndexNodeInt(node->right); // Recursively free right subtree
        free(node); // Free the node itself
    }
}

// Frees the entire database
void freeDatabase(Database *database) {
    freeIndexNodeString((IndexNode*)database->name); // Free name index
    freeIndexNodeString((IndexNode*)database->surname); // Free surname index
    freeIndexNodeString((IndexNode*)database->address); // Free address index
    freeIndexNodeInt((IndexNode*)database->age); // Free age index
}


/*
//Personal version to test these functions
int main() {
    Database db = {0}; // Initialize all indices to NULL
    Persona john = {"John", "Doe", "1234 Maple Street", 28};
    Persona eric = {"Eric", "Eagle", "Via di Torpignattara", 40};

    insert(&db, &john);
    insert(&db, &eric);

    // Find by various attributes
    Persona *foundByName = findByName(&db, "John");
    if (foundByName) printf("Found by name: %s %s\n", foundByName->name, foundByName->surname);
    else printf("Not Found \n");

    foundByName = findBySurname(&db, "Eagle");
    if (foundByName) printf("Found by name: %s %s\n", foundByName->name, foundByName->surname);
    else printf("Not Found \n");

    
    foundByName = findByName(&db, "Mario"); // -> (Not Found!)
    if (foundByName) printf("Found by name: %s %s\n", foundByName->name, foundByName->surname);
    else printf("Not Found \n");

    foundByName = findByAddress(&db, "Via di Torpignattara"); // -> (Not Found!)
    if (foundByName) printf("Found by address: %s %s %s\n", foundByName->name, foundByName->surname, foundByName->address);
    else printf("Not Found \n");

    foundByName = findByAge(&db, 28); // -> (Not Found!)
    if (foundByName) printf("Found by address: %s %s %s\n", foundByName->name, foundByName->surname, foundByName->address);
    else printf("Not Found \n");
    
    freeDatabase(&db);

    return 0;
}
*/