# Train Simulator

This program simulates the operation of trains arriving at and departing from a station with a limited number of tracks. Trains arrive at random intervals and occupy a track for a fixed amount of time.

## Compilation and Execution
To compile the program I propose two methods:
* CMake 
* gcc 

### Using CMake

To compile and run the program using CMake, follow these steps:

1. Ensure CMake is installed on your system.
2. Navigate to the project directory where the `CMakeLists.txt` file is located.
3. Create a build directory and navigate into it:
   ```
   mkdir build && cd build
   ```
4. Run CMake to generate the build system:
   ```
   cmake ..
   ```
5. Compile the program:
   ```
   make
   ```
6. Execute the compiled program:
   ```
   ./TrainSimulator
   ```

### Using GCC Directly

Alternatively, you can compile and run the program directly using `gcc`:

1. Navigate to the project directory.
2. Compile the program with the following command:
   ```
   gcc -o TrainSimulator main.c transit.c -lpthread
   ```
3. Run the compiled program:
   ```
   ./TrainSimulator
   ```

Please note that you need to provide four command-line arguments to run the program: the number of tracks (`tracks`), the time each train occupies a track (`occupancyTime`), and the minimum and maximum interval between train arrivals (`minArrival` and `maxArrival`), respectively.
