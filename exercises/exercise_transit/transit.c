#include "transit.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// Global variables
sem_t availableTracks;
pthread_mutex_t sharedVariablesProtection, trainIdMutex;
int trainsCrossed = 0, trainsCrossing = 0, trainsWaiting = 0;
int trainIdCounter = 0;
int tracks; // Number of tracks
int occupancyTime; // Time a track is occupied in milliseconds
int minArrival, maxArrival; // Time interval between train arrivals

int main(int argc, char* argv[]) {
    // Checking and assigning input parameters
    if (argc != 5) {
        fprintf(stderr, "Usage: %s <tracks> <occupancyTime> <minArrival> <maxArrival>\n", argv[0]);
        return 1;
    }

    tracks = atoi(argv[1]);
    occupancyTime = atoi(argv[2]);
    minArrival = atoi(argv[3]);
    maxArrival = atoi(argv[4]);

    // Initializing semaphores and mutexes
    sem_init(&availableTracks, 0, tracks);
    pthread_mutex_init(&sharedVariablesProtection, NULL);
    pthread_mutex_init(&trainIdMutex, NULL);

    const int NUM_TRAINS = 20; // Number of trains for the simulation
    pthread_t trainThreads[NUM_TRAINS];

    // Creating train threads
    for (int i = 0; i < NUM_TRAINS; ++i) {
        if (pthread_create(&trainThreads[i], NULL, train, NULL) != 0) {
            perror("Failed to create the train thread");
            return 2;
        }
    }

    // Waiting for thread termination
    for (int i = 0; i < NUM_TRAINS; ++i) {
        pthread_join(trainThreads[i], NULL);
    }

    // Cleanup
    sem_destroy(&availableTracks);
    pthread_mutex_destroy(&sharedVariablesProtection);
    pthread_mutex_destroy(&trainIdMutex);

    return 0;
}

void* train(void* arg) {
    pthread_mutex_lock(&trainIdMutex);
    int id = ++trainIdCounter; // Assigning ID to the train
    pthread_mutex_unlock(&trainIdMutex);

    waitForRandom(minArrival, maxArrival);

    pthread_mutex_lock(&sharedVariablesProtection);
    trainsWaiting++;
    printStatus();
    pthread_mutex_unlock(&sharedVariablesProtection);

    sem_wait(&availableTracks);

    pthread_mutex_lock(&sharedVariablesProtection);
    trainsCrossing++;
    trainsWaiting--;
    printStatus();
    pthread_mutex_unlock(&sharedVariablesProtection);

    usleep(occupancyTime * 1000); // Simulating track occupancy

    pthread_mutex_lock(&sharedVariablesProtection);
    trainsCrossing--;
    trainsCrossed++;
    printStatus();
    pthread_mutex_unlock(&sharedVariablesProtection);

    sem_post(&availableTracks);

    return NULL;
}

void waitForRandom(int minMs, int maxMs) {
    int waitTime = minMs + rand() % (maxMs - minMs + 1);
    usleep(waitTime * 1000);
}

void printStatus(void) {
    printf("Trains Crossed: %d, Trains Crossing: %d, Trains Waiting: %d\n", trainsCrossed, trainsCrossing, trainsWaiting);
}
