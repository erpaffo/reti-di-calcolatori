#ifndef TRANSIT_H
#define TRANSIT_H

#include <pthread.h>
#include <semaphore.h>

// Prototipi delle funzioni
void* train(void* arg);
void waitForRandom(int minMs, int maxMs);
void printStatus(void);

#endif // TRANSIT_H
