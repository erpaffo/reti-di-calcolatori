#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 8888
#define MAX_BUFFER 1024
#define BROADCAST_IP "255.255.255.255"

int create_socket() {
    int sockfd;
    int broadcastEnable = 1;

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable)) < 0) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }

    return sockfd;
}

void send_broadcast(int sockfd, int node_id, int sequence_number) {
    struct sockaddr_in broadcastAddr;
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_port = htons(PORT);
    broadcastAddr.sin_addr.s_addr = inet_addr(BROADCAST_IP);

    char message[MAX_BUFFER];
    snprintf(message, sizeof(message), "%d:%d", node_id, sequence_number);

    if (sendto(sockfd, message, strlen(message), 0, (struct sockaddr *)&broadcastAddr, sizeof(broadcastAddr)) < 0) {
        perror("sendto failed");
        exit(EXIT_FAILURE);
    }

    printf("Node %d broadcasted message: %s\n", node_id, message);
}

void listen_for_messages(int sockfd, int node_id) {
    struct sockaddr_in recvAddr;
    socklen_t addrLen = sizeof(recvAddr);
    char buffer[MAX_BUFFER];
    int recvBytes;

    while (1) {
        if ((recvBytes = recvfrom(sockfd, buffer, MAX_BUFFER, 0, (struct sockaddr *)&recvAddr, &addrLen)) < 0) {
            perror("recvfrom failed");
            continue;
        }

        buffer[recvBytes] = '\0'; // Null-terminate the received data.

        int sender_id, received_sequence_number;
        if (sscanf(buffer, "%d:%d", &sender_id, &received_sequence_number) == 2) {
            if (sender_id != node_id) {
                printf("Node %d received message from Node %d: %s\n", node_id, sender_id, buffer);
                // Optional: Logic to handle the received message.
            }
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <NODE_ID> [leader]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int node_id = atoi(argv[1]);
    srand(time(NULL) + node_id); // Ensure different seed for each node.

    int sockfd = create_socket();

    if (argc == 3 && strcmp(argv[2], "leader") == 0) {
        sleep(1); // Wait for other nodes to be ready.
        int sequence_number = rand(); // Generate a random sequence number.
        send_broadcast(sockfd, node_id, sequence_number);
    }

    listen_for_messages(sockfd, node_id);

    close(sockfd);
    return 0;
}
