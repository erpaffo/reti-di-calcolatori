# Reti di Calcolatori
This repository is created and maintained by Francesco Paffetti for the "Reti di Calcolatori" course at "La Sapienza" University.

## Slides
This folder contains materials from [Google Classroom](https://classroom.google.com/u/0/c/NjY1MTQ2ODM3NDI2) of "Reti di Calcolatori" 

## Exercises
This folder contains a collection of programming exercises carried out during the Computer Networks course at La Sapienza University. The exercises cover different topics and concepts covered during the course.


